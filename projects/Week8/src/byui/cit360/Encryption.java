package byui.cit360;

public class Encryption implements Runnable {

    // private variable to hold the passed-in string
    // from the array of strings in main method
    private String stringToEncrypt;

    // private variable to hold length to reduce code line lengths
    private int length;

    // String variable to hold the encrypted result of the encryption method
    private String encryptedString = "";

    // Getter for the encrypted String
    public String getEncryptedString() {
        return encryptedString;
    }

    // constructor of object
    public Encryption(String userString) {
        this.stringToEncrypt = userString;
        this.length = userString.length();
    }
    // pass in (sub)string

    // encrypt (sub)string


    // return

    @Override
    public void run() {
        // increments through string, one character at a time
        for(int i = 0; i < length; i++){

            // gets the i-th character
            char currentChar = stringToEncrypt.charAt(i);

            // convert char to int for encryption/manipulation
            int manipulatorNumber = currentChar;

            // perform mathematical manipulation
            int result = (int) (10 * Math.pow(manipulatorNumber, (2/Math.PI)));

            // cast to char
            char charResult = (char) result;

            // concatenate char to encrypted String
            this.encryptedString += String.valueOf(charResult);

//            System.out.println("The character " + currentChar
//                    + " converts to " + manipulatorNumber + " which is "
//                    + result + " back to "  + charResult);

        }

    }
}
