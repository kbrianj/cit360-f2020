package outdoors.app;

import javax.swing.plaf.nimbus.State;
import java.sql.*;

public class SQLite {

    private static Connection connection;
    private static boolean hasData = false;

//    public ResultSet verifyShopifyValues() throws ClassNotFoundException, SQLException {
//        if (connection == null){
//            getConnection();
//        }
//
//        Statement;
//    }

    /*
        POTENTIAL ISSUES
        - Type mismatching?
     */

    private static void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:outdoors.db");
        initialize();
    }

    private static void initialize()  throws SQLException{
        if(!hasData) {
            hasData = true;


            Statement state = connection.createStatement();
            ResultSet res;
//                    = state.executeQuery(
//                    "SELECT * "+
//                            "WHERE type='table' AND name='brands'");
//            if (!res.next()) {
//                System.out.println("Initializing the database.");
//            }

            Statement createStmnt = connection.createStatement();
            createStmnt.execute("CREATE TABLE brands" +
                    "( brandID integer" +
                    ", brandName VARCHAR(50)" +
                    ", PRIMARY KEY (brandID)); ");

            PreparedStatement prep = connection.prepareStatement(
                    "INSERT INTO brands" +
                            "VALUES(Shopify);"
            );

            res = state.executeQuery(
                    "SELECT brandID " +
                            "FROM brands " +
                            "WHERE brandName = 'Shopify';");

            int shopifyID = res.getInt(0);
/*
//           brandValues table field descriptions
//            brandID INTEGER - brandID from brands table
//            valueName VARCHAR(80) - name of the spreadsheet/csv field/column
//            valueDataType VARCHAR (20) - type of data
//            brandValueOrder INTEGER - order that the fields appear in the vendor file
//                      IS NULLABLE
//            shopifyValueOrder INTEGER - desired place/order of field when mapped to Shopify
                                                                                            */

            createStmnt.execute("CREATE TABLE brandValues " +
                    "( brandID INTEGER" +
                    ", valueName VARCHAR(80)" +
                    ", valueDataType VARCHAR (20)" +
                    ", brandValueOrder INTEGER" +
                    ", shopifyValueOrder INTEGER );");

            prep = connection.prepareStatement("" +
                    "INSERT INTO brandValues" +
                    "(" + shopifyID + ", handle, VARCHAR(99), NULL, 0)"
                    + ",(" + shopifyID + ", title, VARCHAR(99), NULL, 1)"
                    + ",(" + shopifyID + ", bodyHTML, VARCHAR(99), NULL, 2)"
                    + ",(" + shopifyID + ", vendor, VARCHAR(99), NULL, 3)"
                    + ",(" + shopifyID + ", type, VARCHAR(99),NULL, 4)"
                    + ",(" + shopifyID + ", tags, VARCHAR(99),NULL, 5)"
                    + ",(" + shopifyID + ", published_bool, INTEGER,NULL, 6)"
                    + ",(" + shopifyID + ", opt1Name, VARCHAR(99),NULL, 7)"
                    + ",(" + shopifyID + ", opt1Value, VARCHAR(99),NULL, 8)"
                    + ",(" + shopifyID + ", opt2Name, VARCHAR(99),NULL, 9)"
                    + ",(" + shopifyID + ", opt2Value, VARCHAR(99),NULL, 10)"
                    + ",(" + shopifyID + ", opt3Name, VARCHAR(99),NULL, 11)"
                    + ",(" + shopifyID + ", opt3Value, VARCHAR(99),NULL, 12)"
                    + ",(" + shopifyID + ", varSKU, VARCHAR(99),NULL, 13)"
                    + ",(" + shopifyID + ", varGrams, INTEGER,NULL, 14)"
                    + ",(" + shopifyID + ", varInventoryTracker, VARCHAR(99),NULL, 15)"
                    + ",(" + shopifyID + ", varInventoryQty, VARCHAR(99),NULL, 16)"
                    + ",(" + shopifyID + ", varInventoryPolicy_bool, INTEGER,NULL, 17)"
                    + ",(" + shopifyID + ", varFulfillmentService, VARCHAR(99),NULL, 18)"
                    + ",(" + shopifyID + ", varPrice, DOUBLE,NULL, 19)"
                    + ",(" + shopifyID + ", varCompareAtPrice, VARCHAR(99),NULL, 20)"
                    + ",(" + shopifyID + ", varRequiresShipping, VARCHAR(99),NULL, 21)"
                    + ",(" + shopifyID + ", varTaxable, VARCHAR(99),NULL, 22)"
                    + ",(" + shopifyID + ", varBarcode, VARCHAR(99),NULL, 23)"
                    + ",(" + shopifyID + ", imgSrc, VARCHAR(99),NULL, 24)"
                    + ",(" + shopifyID + ", imgPosition, INTEGER,NULL, 25)"
                    + ",(" + shopifyID + ", imgAltText, VARCHAR(99),NULL, 26)"
                    + ",(" + shopifyID + ", giftCard_bool, INTEGER,NULL, 27)"
                    + ",(" + shopifyID + ", SEOTitle, VARCHAR(99),NULL, 28)"
                    + ",(" + shopifyID + ", SEODescription, VARCHAR(99),NULL, 29)"
                    + ",(" + shopifyID + ", GS_GoogleProductCategory, VARCHAR(99),NULL, 30)"
                    + ",(" + shopifyID + ", GS_Gender, VARCHAR(99),NULL, 31)"
                    + ",(" + shopifyID + ", GS_AgeGroup, VARCHAR(99),NULL, 32)"
                    + ",(" + shopifyID + ", GS_MPN, VARCHAR(99),NULL, 33)"
                    + ",(" + shopifyID + ", GS_AdWordsGrouping, VARCHAR(99),NULL, 34)"
                    + ",(" + shopifyID + ", GS_AdWordsLabels, VARCHAR(99),NULL, 35)"
                    + ",(" + shopifyID + ", GS_Condition, VARCHAR(99),NULL, 36)"
                    + ",(" + shopifyID + ", GS_CustomProduct, VARCHAR(99),NULL, 37)"
                    + ",(" + shopifyID + ", GS_CustomLabel0, VARCHAR(99),NULL, 38)"
                    + ",(" + shopifyID + ", GS_CustomLabel1, VARCHAR(99),NULL, 39)"
                    + ",(" + shopifyID + ", GS_CustomLabel2, VARCHAR(99),NULL, 40)"
                    + ",(" + shopifyID + ", GS_CustomLabel3, VARCHAR(99),NULL, 41)"
                    + ",(" + shopifyID + ", GS_CustomLabel4, VARCHAR(99),NULL, 42)"
                    + ",(" + shopifyID + ", VarImg, VARCHAR(99),NULL, 43)"
                    + ",(" + shopifyID + ", VarWeightUnit, VARCHAR(99),NULL, 44)"
                    + ",(" + shopifyID + ", VarTaxCode, VARCHAR(99),NULL, 45)"
                    + ",(" + shopifyID + ", Costperitem, DOUBLE ,NULL, 46);");
        }
    }

    public ResultSet displayBrands() throws ClassNotFoundException, SQLException{
        if (connection == null)
            getConnection();
        Statement statement = connection.createStatement();
        ResultSet results = statement.executeQuery("SELECT brandName FROM brands;");

        return results;
    }

    static void addBrand(String brand) throws ClassNotFoundException, SQLException {
        if(connection == null){
            getConnection();
        }

//  - Insert name of brand into "brands" table
        Statement insertBrand = connection.createStatement();
        insertBrand.execute("INSERT INTO brands " +
                "VALUES (brand);");
    }




//  - Verify brand was written to brands table

    static boolean verifyBrand(String brand) throws ClassNotFoundException, SQLException{
        // Return true if Select "brand" from "brands" != null
        Statement verify = connection.createStatement();
        ResultSet results = verify.executeQuery(
                "SELECT brandName " +
                        "FROM brands " +
                        "WHERE brandName =" + brand + ";");
        if(results != null)
            return true;
        else return false;

    }

/*
     addBrandValues(Queue brandColumns, String brand)
     - use queue to ensure that values are FIFO, to maintain order
     - Insert values into brandValues table where brandID =
        (SELECT brandID WHERE brandName = brand)


/*
     updateBrandValues(Queue brandColumns, String brand)
     - UPDATE reflecting user desired order

*/





//    public
//    if(connection == null){
//        getConnection();
//    }
//    create Brand table

    // {brandID, brandName}

//  create brandValues table
    //  {ValueID?, BrandID, ValueName, OrderInBrandSheet, PositionInShopify}
    // Shopify values get loaded in first
}
