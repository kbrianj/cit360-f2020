import java.nio.file.Path;
import java.util.*;
import java.io.*;

public class main {
    public static void main(String[]args) throws FileNotFoundException {

        Scanner userInput = new Scanner(System.in);

        System.out.println("Hello!\n" +
                "This program will read data from a user-defined CSV or TXT file\n" +
                "and display the results as dictated by the various types from\n " +
                "the Java collections class.");

        Path userFilePath = fileHandling.checks.setFilePath(userInput);

        fileHandling.checks.verifyFilePath(userInput, userFilePath);

        File inputFile = new File(userFilePath.toString());

        if(!inputFile.exists()) {
                System.out.println("Error: File was unable to be found.");
            userFilePath = fileHandling.checks.setFilePath(userInput);
        }

        while (!fileHandling.checks.isCSV(inputFile)
                && !fileHandling.checks.isTXT(inputFile)) {
            try {
                fileHandling.checks.fileFormatType(inputFile);
            } catch (IOException ex) {
                System.out.println("Error: This is not a .csv or .txt file.\n" +
                        "Please use a file with one of those file extensions.");
                userFilePath = fileHandling.checks.setFilePath(userInput);
            }
            inputFile = new File(userFilePath.toString());
        }

        // Verify file can be read
        try {
            fileHandling.checks.checkFileRead(inputFile);
        }
        catch (IOException ex) {
            System.out.println("Error: Cannot read from file.");
            userFilePath = fileHandling.checks.setFilePath(userInput);
        }

        System.out.println("How much of the file would you like to read?\n" +
                    "Note: This may depend on the format of the file you are " +
                    "using.\n" +
                    "1. Read just the first line\n" +
                    "2. Read the whole file");
         int readResponse = Integer.parseInt(userInput.next());

         Boolean firstLineOnly;

         if (readResponse == 1)
             firstLineOnly = true;
         else
             firstLineOnly = false;

        //IF statement to determine delimiter (txt =  space, csv = comma)
            //Read in each value
        if (fileHandling.checks.isCSV(inputFile)) {
            fileHandling.csvFile.displayCollectionComparison(inputFile, firstLineOnly);
        }
        else
            fileHandling.textFile.displayCollectionComparison(inputFile, firstLineOnly);

    }


}