package com.company;


import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public class SpotifyArtist {

    /*
        Spotify API documentation can be found here:
        https://developer.spotify.com/documentation/web-api/reference/
     */


    //Create Artist object type
    private String name;

    //Empty Artist object
    public void SpotifyArtist() {
        name = "";
    }

    // Initialize Artist object with name
    public void SpotifyArtist(String name) {
        this.name = name;
    }

    // Set or change name of the Artist object
    public String getSpotifyArtistName() {
        return name;
    }

    // Method to request input from user and return that input
    public void setSpotifyArtistName(String name) {
        this.name = name;
    }


    // Used https://youtu.be/qzRKa8I36Ww as a reference for the httprequest
    public String searchArtist(String authToken) throws UnsupportedEncodingException {

        UncheckedObjectMapper mapper = new UncheckedObjectMapper();

        // HashMap to store returned JSON data from API
        Map<String, String> spotifyArtistObject;

        String authValue = "Bearer " + authToken;
        HttpClient client = HttpClient.newHttpClient();
        String artistSearchString = "https://api.spotify.com/v1/search"
                + "?q=" + URLEncoder.encode("\"", "UTF-8")
                + this.name.replace(" ", "+")
                + URLEncoder.encode("\"", "UTF-8") + "&type=artist";

        // Build HTTP request using header with Authorization token,
        // and the properly configured URL
        HttpRequest request = HttpRequest.newBuilder()
                .header("Authorization", authValue)
                .uri(URI.create(artistSearchString))
                .build();

        String artistIDString = new String();

        // Perform Spotify search for user provided artist
        // and get results into spotifyArtistObject
        try {
            spotifyArtistObject = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body)
                    .thenApply(mapper::readValue)
                    .get();

            // Parse spotifyArtistObject value from id into artistIDString
            artistIDString = spotifyArtistObject.get("id");
        } catch (InterruptedException e) {
            System.err.println(e);
        } catch (ExecutionException e) {
            System.err.println(e);
        }

        return artistIDString;

    }

    /*public static int getArtistSongs (String authToken, String artistID) {



    }*/

    // Used from https://blogs.oracle.com/java/jdk-http-client
    class UncheckedObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {
        /**
         * Parses the given JSON string into a Map.
         */
        Map<String, String> readValue(String content) {
            try {
                return this.readValue(content, new TypeReference<>() {
                });
            } catch (IOException ioe) {
                throw new CompletionException(ioe);
            }
        }
    }
}


