package outdoors.app;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;


public class Main {



    // Initialize input object for user input
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {


        // Create a File object using the user's input file name
        File inputFile = new File(getUserInputPath());

        // Verify file presence
        try {
            Checks.fileExists(inputFile);
        }
        // Throw FileNotFoundException (with name 'ex') if file is not found
        catch (FileNotFoundException ex) {
            System.out.println("Error:\nInvalid filename.\nFile not found.");
        }

        // Verify file can be read
        try {
            Checks.checkFileRead(inputFile);
        }
        // Throw IOException (with name 'ex') if file cannot be read
        catch (IOException ex) {
            System.out.println("Error:\nCannot read from file.");
        }

        //Declare file object for output file
        File outputFile = new File(getUserOutputPath());

        // System debugging output if file can be read and output name is valid
        System.out.println("Tests passed!\n" + inputFile + " is input.\n"
                + outputFile.toString() +" will be the output file.");


        // Declare variable to hold user file overwrite decision
        String responseString = "y";

        // Instantiate a PrintWriter for the output
        java.io.PrintWriter outputFileWriter;

        // Verify if output file already exists [CHANGE TO TRY/CATCH?]
        while (outputFile.exists()
                &&  responseString.charAt(0) != 'y'){

            // Print message if file is found to already exist
            System.out.println("\nFile with the same path and name that you " +
                    "provided already exists.\nWould you like to overwrite " +
                    "that file? [Yes or No]\t");

            // Get user decision whether or not to overwrite existing file
            responseString = input.next();
            responseString.toLowerCase();

            // Prompt for new file name and/or path
            if(responseString.charAt(0) == 'n') {
                outputFile = new File(getUserOutputPath());
            }
            else if(responseString.charAt(0) != 'y') {
                System.out.println("An invalid response has been entered. " +
                        "Please try again.");
            }
        }



        // Declare variable to hold type of spreadsheet file
        Boolean spreadsheetFileType = null;

        // Determine spreadsheet type (.xlsx = T or .xls = F)
        try {
            spreadsheetFileType = Checks.fileFormatType(inputFile);
        }
        // Throw excpetion if not an MS Office spreadsheet file type
        catch (IOException ex) {
            System.out.println("Error:\nUnspported File type or extension");
        }

        FileInputStream fileStream;
        ArrayList<String> brandValues;

        // Read in first line to verify the field format
//        try {
//            fileStream = new FileInputStream(inputFile);
//            if(spreadsheetFileType)
//                    brandValues = XLSXHandling.readFirstLine(fileStream);
//            else
//                brandValues = XLSHandling.readFirstLine(fileStream);
//
//        } catch (FileNotFoundException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }

        SQLite sqlDB = new SQLite();
        ResultSet sqlResults;

        try {
            sqlResults = sqlDB.displayBrands();
            while (sqlResults.next()){
                System.out.println(sqlResults.getString("brandName"));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }


        // Iterate through each row in the file (via Handling classes)



        // Create output file if does not already exist
        try {
            outputFileWriter = new PrintWriter(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* public static String xlsxHandling(String input) {

    } */

    /*
        public static String xlsHandling(String input) {

         }
     */

    public static String getUserInputPath() {
        // Request user's input file path
        System.out.println("Please enter the path (including the filename) of " +
                "the INPUT file:");

        // Get user's input file path
        String inputFileName = input.next();

        return inputFileName;
    }

    public static String getUserOutputPath() {
        // Request user's desired output file name and path
        System.out.println("Please enter the path (including the filename) of " +
                "the OUTPUT file:");

        // Get user's desired output file name and path
        String outputFileName = input.next();

        return outputFileName;
    }

}
