package byui.cit360;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

/* Overall program layout/plan:
    // get string

    // create substrings of 3-6 chars

    // Encrypt substrings (each in a concurrence string)

    // get encrypted substrings

    // concatenate encrypted substrings

    // re-encrypt string of concatenated substrings

    // return re-encryption string


 */

    public static void main(String[] args) {
        // Display program description
        System.out.println("This program will take a user-provided string\n" +
                "and return an encrypted form of that string.\n");

        // Request input from user
        System.out.println("Please enter a word or other\n" +
                "information you would like to encrypt (without spaces):");

        // Scanner to read in input from user
        Scanner ioScanner = new Scanner(System.in);

        // Test 27 char string for debugging
//        String userInput = "This is a long test string!" ;

        // Scanner input for production use
        String userInput = ioScanner.next();

        // Notify user of process initiation
        System.out.println("Please wait while your information is encrypted.");

        // Create array of substrings using createSubStrings method
        ArrayList<String> userSubstrings = createSubStrings(userInput
                , findOptimalSubStringLength(userInput));

//        for (String i : userSubstrings) {
//            System.out.println(i);
//        }

        // Empty string to concatenate results into
        String encryptedResult = "";


        ExecutorService runTheRunnables = Executors.newFixedThreadPool(2);
        ArrayList<Encryption> encryptions = new ArrayList<>();

        for(String s : userSubstrings) {
            int i = 0;
            encryptions.add(i, new Encryption(s));
            runTheRunnables.execute(encryptions.get(i));
            i++;
        }

        try {
            runTheRunnables.awaitTermination(100, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        for(Encryption e : encryptions){
            encryptedResult += e.getEncryptedString();
        }



        System.out.println("The encryption of " + userInput
            + " results in " + encryptedResult);

        runTheRunnables.shutdown();

    }

    public static int findOptimalSubStringLength(String userData) {
        int[] smallStringDivisors = {2, 3, 4, 5};
        int[] largeStringDivisors = {5, 6, 7, 8, 9};
        int mostEvenDivisor = 9;
        int largestRemainder = 0;
//        System.out.println("The length of the string is " + userData.length());


        // Determine optimal substring length for smaller strings
        if(userData.length() < 11) {
            // Loop through all smaller divisors
            for (int i: smallStringDivisors) {

//                System.out.println("The result of " + userData.length()
//                        + "%" + i + "is: "  + (userData.length() % i) );
                if (userData.length() % i == 0) {
                    mostEvenDivisor = i;
                }
                else if ( (userData.length() % i) > largestRemainder) {

                    largestRemainder = userData.length() % i;

                    // Assign divisor that results in
                    // largest remainder as optimal Divisor
                    mostEvenDivisor = i;

                    // Testing output line
//                    System.out.println("The most even divisor is: " + mostEvenDivisor);
                }
            }
        }
        // Determine optimal substring length for bigger inputs
        else {
            // Loop through all smaller divisors
            for (int i: largeStringDivisors) {
//                System.out.println("The result of " + userData.length()
//                        + "%" + i + "is: "  + (userData.length() % i) );
                if (userData.length() % i == 0) {
                    mostEvenDivisor = i;
                }

                else if ( (userData.length() % i) > largestRemainder) {
                    largestRemainder = userData.length() % i;

                    // Assign divisor that results in
                    // largest remainder as optimal Divisor
                    mostEvenDivisor = i;
                }
            }
        }

        return mostEvenDivisor;
    }

    // This method divides the user's string into multiple substrings
    public static ArrayList<String> createSubStrings(String userData
            , int mostEvenDivisor) {

        // spaces to fill out grid of character array
        String additionalSpaces = " ";

        // Add spaces to end of string only if the string
        // could not be evenly divided
        if ( (userData.length() % mostEvenDivisor) > 0) {
            // Add only the number of spaces needed to fill the last row of array
            userData += additionalSpaces.repeat(
                    mostEvenDivisor - (userData.length() % mostEvenDivisor));
        }

        // Testing output to show length after adding spaces
//        System.out.println("The length of the string is now " +
//                userData.length());

        // Initialize empty array of substrings
        ArrayList<String> substrings = new ArrayList();

        // break down passed in string into an array of substrings
        // using the length found by findOptimalSubStringLength
        int stringIndex = 0;
        for(int i = 0; stringIndex < userData.length(); i++) {
            substrings.add(i, userData.substring(stringIndex
                    , stringIndex + mostEvenDivisor));
            stringIndex += mostEvenDivisor;
        }

        return substrings;
    }
}
