package outdoors.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Checks {

    public static boolean fileFormatType(File testFile) throws IOException {
        String fileName = testFile.toString();
        if (fileName.endsWith("xlsx"))
            return true;
        else if (fileName.endsWith("xls"))
            return false;
        else
            throw new IOException("File is in an invalid format");
    }

    public static void fileExists(File testFile) throws FileNotFoundException {
        if(!testFile.exists())
            throw new FileNotFoundException("File does not exist.");

    }

    public static void checkFileRead(File testFile) throws IOException {
        if (!testFile.canRead())
            throw new IOException("File cannot be read.");
    }
}
