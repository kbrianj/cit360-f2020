package com.company;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Display program description
        System.out.println("This program will retrieve a musical artist's " +
                "name from the user and then return the number of that " +
                "artist's songs available on the Spotify streaming platform");

        // Create artist object and
        // call setArtist method to set artist name
        SpotifyArtist userArtist = new SpotifyArtist();
        System.out.print("Please enter a musical artist to search for: ");
        Scanner userInput = new Scanner(System.in);
        userArtist.setSpotifyArtistName(userInput.nextLine());


        // Get authorization from Spotify API
        String authToken = SpotifyRequest.getSpotifyAuth();

        // String to hold Spotify artist id string to pass between methods
        String artistID = "";

        // Initialize "guard" variable to control loop of artist validation
        boolean artistFound = false;

        // While guard variable is false, use try/catch for exceptions
        while(!artistFound) {

            try {
                artistID = userArtist.searchArtist(authToken);
                artistFound = true;
                // Try calling searchArtist method to verify
                // presence of artist in Spotify database
            }

            // Catch exception when no artist is found
            catch (UnsupportedEncodingException e) {
                System.out.println("An exact match was not found for an artist" +
                        "\nwith the name " + userArtist.getSpotifyArtistName() + "." +
                        "\nPlease try again.");
            }

        }

        // Call getArtistSongs method

        // Display number of artist's songs available in Spotify
        /*System.out.println("The artist " + userArtist.getSpotifyArtistName()
        + " has " + userArtist.getArtistSongs(authToken)
                + " songs available in the Spotify database.");*/
    }
}
