package byui.cit360;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    // Test using assertEquals
    @Test
    void testReadValues() {

        ArrayList<Student> studentValues = writeValuesFromFile();

        // Verify that missing grades are saved as 0.0
        assertEquals(0.0, studentValues.get(4).getGrade());

        // Verify that missing ID number are saved as 0
        assertEquals(000000, studentValues.get(7).getIdNumber());
    }

    // Test using assertTrue
    @Test
    void testNumStudents() {
        ArrayList<Student> studentValues = writeValuesFromFile();

        assertTrue(studentValues.size() == 10);

        System.out.println("The number of students in the file matches the\n" +
                "number of students in the stored ArrayList.");
    }

    // Test using assertNotSame
    @Test
    void testNoDuplicates() {

        ArrayList<Student> studentValues = writeValuesFromFile();


        // Compare each entry in the Student ArrayList against all
        // other entries in the ArrayList
        for (int i = 0; i < (studentValues.size() - 1); i++)
        {
            /*
                Logic behind this for loop structure:
                If the i-th index of the Array list is being compared,
                only those entries that come after it need to be compared
                eg. for the first entry, it must be compared against entries
                2-10, for the second 3-10, and so on.
             */

            for (int j = i+1; j < (studentValues.size() - 1); j ++) {
                if (i != j) {
                    assertNotSame(studentValues.get(i), studentValues.get(j));
                }
            }
        }

        System.out.println("There are no duplicate entries in the file.");

    }

    // Test using assertNull
    @Test
    void testMissingName() {
        ArrayList<Student> studentValues = writeValuesFromFile();

        assertNull(studentValues.get(0).getName());

        System.out.println("The first student currently has a null " +
                "assignment for a name.");
    }

    // Test using assertNotNull
    @Test
    void testPresenceOfValues() {

        ArrayList<Student> studentValues = writeValuesFromFile();

        for (int i = 0; i < studentValues.size(); i++) {
            if(i != 0) {
                assertNotNull(studentValues.get(i).getName());
            }
            assertNotNull(studentValues.get(i).getIdNumber());
            assertNotNull(studentValues.get(i).getGrade());
        }

        System.out.println("With the exception of the name of the first " +
                "student,\nall students have valid values for each field");
    }


    public ArrayList<Student> writeValuesFromFile () {
        // Instantiate file object with test file location
        File inputFile = new File(createTestFilePath());

        // Initialize file scanner object
        Scanner fileInput = null;

        // Verify that scanner can read file
        try {
            fileInput = new Scanner(inputFile);
        }
        catch (FileNotFoundException e) {
            System.err.println(e);
            System.out.println("Test file not found." +
                    "\nNow exiting.");
            System.exit(-1);
        }

        // Initialize ArrayList for all Student values
        ArrayList<Student> studentArray = new ArrayList<>();

        // Read in student values from test text file
        while (fileInput.hasNextLine()) {
            // Grab each line of file
            String line = fileInput.nextLine();

            // Create scanner for the line
            Scanner lineScanner = new Scanner(line);

            // Initialize variable with default values
            String inputName = null;
            int inputID = 000000;
            double inputGrade = 0.0;

            // Read in first value as a String (should be name)
            if(isAlpha(lineScanner.next())){
                inputName = lineScanner.next();
            }

            // Read in next Integer value (should be student ID)
            if(lineScanner.hasNextInt()) {
                inputID = lineScanner.nextInt();
            }

            // nextDouble was not picking up double from file,
            // and was getting passed to next entry in ArrayList
            if(lineScanner.hasNextDouble()) {
                inputGrade = lineScanner.nextDouble();
            }

            // Save values to Student variable
            Student studentFileInput = new Student(
                    inputName, inputID, inputGrade);

            // Add values as a Student object to ArrayList
            studentArray.add(studentFileInput);
        }

        return studentArray;
    }

    public String createTestFilePath() {
        // Get path of where file is being run
        String pathBuilder = new File("").getAbsolutePath();

        // Append the test text file location
        pathBuilder = pathBuilder.concat("\\src\\test.txt");

        return pathBuilder;
    }

    public Boolean isAlpha (String info) {
        return info.matches("[a-zA-Z]+");
    }

}