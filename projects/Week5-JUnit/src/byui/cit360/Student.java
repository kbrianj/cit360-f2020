package byui.cit360;

public class Student {
    private String name;
    private int idNumber;
    private double grade;

    public Student () {

    }
    public Student(String name, int idNumber, double grade) {
        this.name = name;
        this.idNumber = idNumber;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}
