import java.util.Scanner;
import java.lang.System;

public class Main {

    public static void main(String[] args) {

        System.out.println("This program will accept two numerical values\n" +
                "provided by the user and display the quotient.");

        Scanner userInput = new Scanner(System.in);
        System.out.print("Please enter two numbers to divide:");

        double num1 = Double.parseDouble(userInput.next());
        double num2 = Double.parseDouble(userInput.next());

            try {
                divisorChecker(num2);
            }
            catch (ArithmeticException ex) {
                while (num2 == 0.0) {
                    System.out.print("Please enter another number for the divisor: ");
                    num2 = Double.parseDouble(userInput.next());
                }
            }

        double result = twoNumDivision(num1, num2);

        System.out.println("The division of " + num1 + " by " + num2
                + " is " + result );



    }

    public static double twoNumDivision (double num1, double num2)
        throws ArithmeticException{

        return num1 / num2;
    }


    public static void divisorChecker (double num) throws ArithmeticException{
        if (num == 0.0)
            throw new ArithmeticException("Error: Divisor cannot be zero.");
    }


}
