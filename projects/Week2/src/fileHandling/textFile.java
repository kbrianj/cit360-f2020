package fileHandling;

import java.io.*;
import java.util.*;

public class textFile {

        public static void displayCollectionComparison (File inputFile
                , Boolean lineOnly) throws FileNotFoundException {

            // Get value count based on whether line only or whole file
            int fileValuesNum;
            if (lineOnly)
                fileValuesNum = countFileValues(inputFile);
            else
                fileValuesNum = countFirstLineValues(inputFile);

            // Call setTree method
            TreeMap fileTree = setTree(inputFile, lineOnly);

            // Call setList method
            ArrayList<String> fileList = setList(inputFile, lineOnly);

            // Call setQueue method
            PriorityQueue<String> fileQueue = setQueue(inputFile, lineOnly);

            // Call setSet method
            Set<String> fileSet = setSet(inputFile, lineOnly);

            // Make sure to use generics

            // Make sure to use Comparator interface


        /* Get length of longest entry from file, add
         2 for space between columns */
            int longestFileEntry = getLongestEntry(fileTree) + 2;

            String space = " ";

            String tableHead =
                    "Tree" + space.repeat(longestFileEntry - 4)
                            + "List" + space.repeat(longestFileEntry - 4)
                            + "Set" + space.repeat(longestFileEntry - 3)
                            + "Queue";

            // Print table heading
            System.out.println("Comparison of the storage of different " +
                    "Collection types\n" + tableHead);

            System.out.println("-".repeat(longestFileEntry * 4));

            // Iterate through collections & print to console
            // Iterator<String> iList = fileList.iterator();
            Iterator<String> iQueue = fileQueue.iterator();
            Iterator<String> iSet = fileSet.iterator();

            // Loop through values
            for(int i = 0; i < fileValuesNum; i++) {
                String dummyString;

                System.out.print(
                        fileTree.get(i).toString()
                                + space.repeat(longestFileEntry - fileTree.get(i).toString().length())
                                + fileList.get(i)
                                + space.repeat(longestFileEntry - fileList.get(i).length()));


                if(iSet.hasNext()) {
                    try {
                        dummyString = space.repeat(longestFileEntry - iSet.next().length());
                        System.out.print(iSet.next() + dummyString);
                    } catch (NoSuchElementException ex) {
                        System.out.print(space.repeat(longestFileEntry));
                    }
                }

                if (iQueue.hasNext()){
                    try {
                        System.out.print(iQueue.next() + "\n");
                    }
                    catch (NoSuchElementException ex) {
                        System.out.print (space.repeat(longestFileEntry) + "\n");
                    }
                }
            }

        }

        public static int getLongestEntry (TreeMap tree) {
            int longestEntry = 0;
            for(int i = 0; i < tree.size(); i++){
                String holder = tree.get(i).toString();

                if (longestEntry < holder.length())
                    longestEntry = holder.length();
            }

            return longestEntry;
        }


        public static Set<String> setSet (File inputFile, Boolean lineOnly)
                throws FileNotFoundException {
            Scanner fileInputScanner = new Scanner(inputFile);

            // Establish set collection
            Set<String> fileSet = new LinkedHashSet<>();

            if(lineOnly) {
                for (int i = 0; i < countFirstLineValues(inputFile); i++) {
                    fileSet.add(fileInputScanner.next());
                }
            }
            else {
                for (int i = 0; i < countFileValues(inputFile); i++){
                    fileSet.add(fileInputScanner.next());
                }
            }

            return fileSet;
        }

        public static PriorityQueue<String> setQueue (File inputFile, Boolean lineOnly)
                throws FileNotFoundException {

            Scanner fileInputScanner = new Scanner(inputFile);

            // Establish Priority Queue collection
            PriorityQueue<String> fileQueue = new PriorityQueue<>();

            if(lineOnly) {
                for (int i = 0; i < countFirstLineValues(inputFile); i++) {
                    fileQueue.add(fileInputScanner.next());
                }
            }
            else {
                for (int i = 0; i < countFileValues(inputFile); i++){
                    fileQueue.add(fileInputScanner.next());
                }
            }

            fileInputScanner.close();

            return fileQueue;

        }

        public static ArrayList<String> setList (File inputFile, Boolean lineOnly)
                throws FileNotFoundException {
            Scanner fileInputScanner = new Scanner(inputFile);

            // Establish set collection
            ArrayList<String> fileList = new ArrayList<>();

            if(lineOnly) {
                for (int i = 0; i < countFirstLineValues(inputFile); i++) {
                    fileList.add(fileInputScanner.next());
                }
            }
            else {
                for (int i = 0; i < countFileValues(inputFile); i++){
                    fileList.add(fileInputScanner.next());
                }
            }

            fileInputScanner.close();

            return fileList;
        }

        public static TreeMap setTree (File inputFile, Boolean lineOnly)
                throws FileNotFoundException{
            Scanner fileInputScanner = new Scanner(inputFile);

            // Establish set collection
            TreeMap<Integer,String> fileTree = new TreeMap<>();

            if(lineOnly) {
                for (int i = 0; i < countFirstLineValues(inputFile); i++) {
                  fileTree.put(i,fileInputScanner.next());
                }
            }
            else {
                for (int i = 0; i < countFileValues(inputFile); i++){
                    fileTree.put(i,fileInputScanner.next());
                }
            }

            fileInputScanner.close();

            return fileTree;
        }

        public static int  countFileValues (File inputFile)
                throws FileNotFoundException {

            Scanner lineScanner = new Scanner(inputFile);
             
            int valueCount = 0;

            while(lineScanner.hasNext() && lineScanner.hasNextLine()) {
                String holder = lineScanner.next();
                valueCount++;
            }
            lineScanner.close();

            return valueCount;
        }

        public static int countLineValues (File inputFile)
                throws FileNotFoundException {
            Scanner fileScanner = new Scanner(inputFile);
            int lineCount = 0;
            int firstLineCount = 0;
            while(fileScanner.hasNextLine()) {
                String lineToRead = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(lineToRead);
                 
                int valueCount = 0;
                lineCount++;
            }
            fileScanner.close();

            return lineCount;
        }

        public static int countFirstLineValues (File inputFile)
                throws FileNotFoundException {
            Scanner fileScanner = new Scanner(inputFile);
            int lineCount = 0;
            int firstLineCount = 0;
            while(fileScanner.hasNextLine()) {
                String lineToRead = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(lineToRead);
                 
                int valueCount = 0;

                while (lineScanner.hasNext() /*&& valueCount < 25*/) {
                    String valueHolder = lineScanner.next();
                    valueCount++;
                    if(lineCount <1) {
                        firstLineCount++;
                    }
                }
                lineCount++;
            }

            fileScanner.close();
            return firstLineCount;
        }

    }

