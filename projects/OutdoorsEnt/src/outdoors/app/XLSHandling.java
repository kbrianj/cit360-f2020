package outdoors.app;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class XLSHandling {
    static public ArrayList<String> readFirstLine(FileInputStream inputStream) throws IOException {
        Row row;
        ArrayList<String> brandValues = new ArrayList<String>();

        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        row = (Row) rowIterator.next();
        Iterator<Cell> cellIterator = row.cellIterator();
        while(cellIterator.hasNext()) {
            Cell cellContents = cellIterator.next();
            brandValues.add(cellContents.getStringCellValue());
        }

        return brandValues;
    }
}
