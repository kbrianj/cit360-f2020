package com.company;

import com.fasterxml.jackson.databind.util.JSONPObject;

import javax.net.ssl.HttpsURLConnection;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.HashMap;
import java.io.*;
import java.util.Base64;
import java.util.StringJoiner;

public class SpotifyRequest {
    /*
        Documentation for the Spotify API authorization process can be found here:
        https://developer.spotify.com/documentation/general/guides/authorization-guide/#client-credentials-flow
     */


    public static String getSpotifyAuth() {

        String spotifyURL = "https://accounts.spotify.com/api/token";

        BufferedReader responseReader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        int statusResponse = 0;

        try {
            URL spotifyAPI = new URL(spotifyURL);
            HttpsURLConnection connection = (HttpsURLConnection) spotifyAPI.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", "Basic " + buildRequest());
            connection.setDoOutput(true);
            DataOutputStream stream = new DataOutputStream(connection.getOutputStream());
            String requestBody = "grant_type=client_credentials";

            stream.writeBytes(requestBody);

            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            statusResponse = connection.getResponseCode();
            stream.flush();
            stream.close();
            if (statusResponse != 200) {
                System.out.println("Error: could not connect to server.");
            }
            else {
                System.out.println("Connected! Connection status is: " + statusResponse);
                responseReader  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ( (line = responseReader.readLine()) != null) {
                    responseContent.append(line);
                }

                responseReader.close();
            }

            System.out.println(responseContent.toString());
        }
        catch (IOException e) {
            System.err.println( e.toString());
        }


        int tokenIndex = responseContent.indexOf(":") + 2;
        return responseContent.substring(tokenIndex, responseContent.indexOf("\"",tokenIndex));
    }

    public static String buildRequest() {
        String clientID = "7f85e37c4adf405d9dd665d57632a30b";
        String clientSecret = "fb079262c808444f8bf54491df51950d";

        return Base64.getEncoder().encodeToString(
                clientID.concat(":"+clientSecret).getBytes());

    }


}


